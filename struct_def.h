/* Robert Lowell RL659896 */
/* CSI402 Project 4       */
/* 4/15/13                */
/* struct_def.h           */

#ifndef STRUCT_DEF_H
#define STRUCT_DEF_H

#include "constants.h"

/* Symbol List Structure */
typedef struct symnode {
  char symName[MAX_SYMBOL_LENGTH]; int lcValue;
  struct symnode *next;
} *SYMNODE;

/* Error List Structure */
typedef struct errnode {
  int lineNum;
  char errStr[MAX_SYMBOL_LENGTH];
  struct errnode *next;
} *ERRNODE;

/* MOT Struct */
typedef struct motstruct {
  char opCode[NUM_OP_CODES][OP_CODE_LENGTH]; 
  char opName[NUM_OP_CODES][OP_NAME_LENGTH];
  char opType[NUM_OP_CODES][TYPE_LENGTH];
} *MOTSTRUCT;


#endif
