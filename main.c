/* Robert Lowell RL659896 */
/* CSI402 Project 4       */
/* 4/15/13                */
/* main.c                 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "constants.h"
#include "struct_def.h"
#include "prototypes.h"

void printSym(SYMNODE, char *, int);
void printObj(int[], int, char *, int);
void printErr(ERRNODE, ERRNODE, ERRNODE, FILE *, char *, int);


/* Main Program Function */
int main(int argc, char *argv[]){
  /* Read in mot.txt and create opcode struct. Using a Two-pass assembler, First pass:  */
  /* Read input .asm line by line populating symbol table. Second pass: Rewind to top   */
  /* of input .asm and read each line and strtok to parse operands.                     */
  /* Call the appropriate function to handle operand's hex instruction creation.        */

  FILE *inputFile, *motFile;
  int countLC = 0, dataFlag = 0, errorFlag = 0, hadError = 0, lineNum = 0;
  int objArray[65536], objIndex = 0;
  int i = 0, j = 0, filenameLength = 0;
  char *token;
  char tempStr[MAX_LINE_LENGTH];

  SYMNODE symbolList = NULL, tempSymNode = NULL, newSymNode = NULL;
  ERRNODE duplicateList = NULL, undefinedList = NULL, unknownList = NULL;
  ERRNODE tempErrNode = NULL, newErrNode = NULL;
  MOTSTRUCT motStruct = NULL;

  /* Check for correct number of arguements. */
  if(argc != NUM_ARGS){
    fprintf(stderr, "Arguement usage: p4 inputfile.asm\n"); fflush(stderr);
    exit(1);
  }

  /* Get the input filename's length. */
  filenameLength = strlen(argv[IN_FILE_ARG]);


  /* Allocate memory for motStruct. */
  if((motStruct = (struct motstruct *)malloc(sizeof(struct motstruct))) == NULL){
    fprintf(stderr, "MOT Struct allocation failed.\n"); fflush(stderr);
    exit(1);
  }

  /* Read in mot.txt and populate motStruct */
  if((motFile = fopen(MOT_FILE, "r")) == NULL){
    /* Open Failed */
    fprintf(stderr, "File: %s could not be opened for reading.\n", MOT_FILE); fflush(stderr);
    exit(1);
  }

  /* Parse motFile line by line, store data in motStruct. */
  i = 0;
  while(fgets(tempStr, MAX_LINE_LENGTH, motFile) != NULL){
    /* Grab opcode number. */
    token = strtok(tempStr, " \n");
    /* Store hex opcode in struct. */
    strncpy(motStruct->opCode[i], token, OP_CODE_LENGTH);
    
    /* Grab opcode Mneumonic. */
    token = strtok(NULL, " \n");
    /* Store mneumonic. */
    strncpy(motStruct->opName[i], token, OP_NAME_LENGTH);

    /* Grab opcode type. */
    token = strtok(NULL, " \n");
    /* Store type. */
    strncpy(motStruct->opType[i], token, TYPE_LENGTH);
    i++;
  }

  /* Close motFile. */
  if(fclose(motFile) == EOF){
    fprintf(stderr, "Error closing file %s\n", MOT_FILE); fflush(stderr);
  }

  /* Open .asm file for first-pass parsing. Errors to be detected:  duplicate defined symbols. */
  if((inputFile = fopen(argv[IN_FILE_ARG], "r")) == NULL){
    /* Open Failed */
    fprintf(stderr, "File: %s could not be opened for reading.\n", argv[IN_FILE_ARG]); fflush(stderr);
    exit(1);
  }

  while(fgets(tempStr, MAX_LINE_LENGTH, inputFile) != NULL){
    /* Check for .data. If we are not in a .data section, check the first index of each line */
    /* to see if a character other than # exists. If so, we have a symbol to be stored.      */
    /* Check the symbol list for any duplicates and also count LC-values. If duplicate a     */
    /* duplicate is found, flag that an error has been found and proceed to look for any more*/
    /* duplicates. If .data is found, continue to check for symbols, but also read in .resw  */
    /* or .word to calculate the correct LC value.                                           */

    /* Increment line count. */
    lineNum++;

    /* Check to see if the first character is a '#' to determine if it is a comment line.    */
    if(tempStr[0] != '#'){
      /* Check to see if the line is blank. */
      token = strtok(tempStr, " :\n");
      if(token != NULL){
	/* Line is not blank */
        /* Check to see if we need to check for .data directive. */
        if(dataFlag == 0){
        /* Check to see if the first character is whitespace or not. */
	if(tempStr[0] == ' '){
	    /* Check if the line was the .data directive. */
	    if(strcmp(token, ".data") == 0){
	      /* We are in the .data portion of the file. Check for .resw and .word directives. */
	      dataFlag = 1;
	    }
	    else if(strcmp(token, ".text") != 0){
	      /* Line was not a comment, it was not blank, it did not have a directive or symbol. */
	      /* The line must have some operand. Increase LC value by 1.                         */
	      countLC++;
	    }
	}
	else{
	  /* First character isn't '#' or whitespace, it has to be a label. */
	  /* Check list for duplicates, if duplicate is found flag an error.*/
	  /* Else add it to the list.                                       */
	  
	  /* Check if our symbol list is empty. */
	  if(symbolList == NULL){
	    /* List is empty, make a new node for the head of the list. */
	    if((symbolList = (struct symnode *)malloc(sizeof(struct symnode))) == NULL){
	      fprintf(stderr, "symNode allocation failed.\n"); fflush(stderr);
	      exit(1);
	    }

	    /* Store symbol and LC value in new node. */
	    strncpy(symbolList->symName, token, 20);
	    symbolList->lcValue = countLC;
	  }
	  else{
	    /* Symbol list is not empty, check the list for any duplicates. */
	    tempSymNode = symbolList;
	    for(; tempSymNode != NULL; tempSymNode = tempSymNode->next){
	      if(strcmp(tempSymNode->symName, token) == 0){
		/* A duplicate is found, flag an error and store the duplicate in the error list. */
		hadError = 1;
		errorFlag = 1;
		
		/* Check to see if the error list is empty. */
		if(duplicateList == NULL){
		  /* Duplicate list is empty, create a new node. */
		  if((duplicateList = (struct errnode *)malloc(sizeof(struct errnode))) == NULL){
		    fprintf(stderr, "errNode allocation failed.\n"); fflush(stderr);
		    exit(1);
		  }
		  
		  /* Store info in the error node. */
		  duplicateList->lineNum = lineNum;
		  strncpy(duplicateList->errStr, token, MAX_SYMBOL_LENGTH);
		}
		else{
		  /* Error list isn't empty, append new node. */
		  tempErrNode = duplicateList;
		  
		  /* Go to the end of the error list. */
		  for(; tempErrNode->next != NULL; tempErrNode = tempErrNode->next);
		  
		  /* Create a new node. */
		  if((newErrNode = (struct errnode *)malloc(sizeof(struct errnode))) == NULL){
                    fprintf(stderr, "errNode allocation failed.\n"); fflush(stderr);
                    exit(1);
                  }
		  
		  /* Put information in the new node. */
		  newErrNode->lineNum = lineNum;
		  strncpy(newErrNode->errStr, token, MAX_SYMBOL_LENGTH);

		  /* Put new node on the end of the list. */
		  tempErrNode->next = newErrNode;
		}
	      }
	    }
	    if(hadError == 0){
		/* No duplicate found. Add new symbol node to the end of the list. */
		tempSymNode = symbolList;
		for(; tempSymNode->next != NULL; tempSymNode = tempSymNode->next);

		if((newSymNode = (struct symnode *)malloc(sizeof(struct symnode))) == NULL){
		  fprintf(stderr, "symNode allocation failed.\n"); fflush(stderr);
		  exit(1);
		}
		/* Store symbol and LC value in new node. */
		strncpy(newSymNode->symName, token, MAX_SYMBOL_LENGTH);
		newSymNode->lcValue = countLC;
		/* Add node to the list. */
		tempSymNode->next = newSymNode;
	    }
	    else{
	      hadError = 0;
	    }
	  }
	  /* Increment LC value by 1 since we are not in the .data directive */
          /* so there is no .resw or .word to worry about.                   */
          countLC++;
	}
      }
      else{
	/* We are already in .data, and not a comment line. See if the first character is not white space.    */
	/* If the first character is a whitespace character and in the .data segment that means a blank line. */
	if(tempStr[0] != ' '){
	  /* Check for duplicate defined symbols. */
	  /* Check if our symbol list is empty. */
          if(symbolList == NULL){
            /* List is empty, make a new node for the head of the list. */
            if((symbolList = (struct symnode *)malloc(sizeof(struct symnode))) == NULL){
              fprintf(stderr, "symNode allocation failed.\n"); fflush(stderr);
              exit(1);
            }

            /* Store symbol and LC value in new node. */
            strncpy(symbolList->symName, token, MAX_SYMBOL_LENGTH);
            symbolList->lcValue = countLC;
          }
          else{
            /* Symbol list is not empty, check the list for any duplicates. */
            tempSymNode = symbolList;
            for(; tempSymNode != NULL; tempSymNode = tempSymNode->next){
	      if(strcmp(tempSymNode->symName, token) == 0){
                /* A duplicate is found, flag an error and store the duplicate in the error list. */
                errorFlag = 1;
		hadError = 1;
	        /* Check to see if the error list is empty. */
                if(duplicateList == NULL){
                  /* Error list is empty, create a new node. */
                  if((duplicateList = (struct errnode *)malloc(sizeof(struct errnode))) == NULL){
                    fprintf(stderr, "errNode allocation failed.\n"); fflush(stderr);
                    exit(1);
                  }

                  /* Store info in the error node. */
                  duplicateList->lineNum = lineNum;
                  strncpy(duplicateList->errStr, token, MAX_SYMBOL_LENGTH);
                }
                else{
	          /* Error list isn't empty, append new node. */
                  tempErrNode = duplicateList;

                  /* Go to the end of the error list. */
                  for(; tempErrNode->next != NULL; tempErrNode = tempErrNode->next);

                  /* Create a new node. */
                  if((newErrNode = (struct errnode *)malloc(sizeof(struct errnode))) == NULL){
                    fprintf(stderr, "errNode allocation failed.\n"); fflush(stderr);
                    exit(1);
                  }

                  /* Put information in the new node. */
                  newErrNode->lineNum = lineNum;
                  strncpy(newErrNode->errStr, token, MAX_SYMBOL_LENGTH);

                  /* Put new node on the end of the list. */
                  tempErrNode->next = newErrNode;
                }
              }
	    }
            if(hadError == 0){
	      tempSymNode = symbolList;
              /* No duplicate found. Add new symbol node to the end of the list. */
              for(; tempSymNode->next != NULL; tempSymNode = tempSymNode->next);

              if((newSymNode = (struct symnode *)malloc(sizeof(struct symnode))) == NULL){
	        fprintf(stderr, "symNode allocation failed.\n"); fflush(stderr);
                exit(1);
              }

              /* Store symbol and LC value in new node. */
              strncpy(newSymNode->symName, token, MAX_SYMBOL_LENGTH);
              newSymNode->lcValue = countLC;

              /* Add node to the list. */
              tempSymNode->next = newSymNode;
	    }
	    else{
	      hadError = 0;
	    }
          }
	  
	  /* Parse the rest of the line to see if we have a .resw or a .word directive. */
	  token = strtok(NULL, " :\n");

	  /* Check if it is a .word directive. */
	  if(strcmp(token, ".word") == 0){
	    /* It is a .word directive, see how many are created to get the correct LC value. */
	    /* This is the first part of the .word directive, currently is not important to us. */
	    token = strtok(NULL, " :\n");

	    /* This is the second part of the .word directive.   */
	    /* This is the number we should add to the LC count  */
	    token = strtok(NULL, " :\n");
	    
	    /* Convert token from string to int. */
	    i = 0;
	    i = atoi(token);
	    
	    /* Add the value to our LC count. */
	    countLC += i;	    
	  
	  }
	  else if(strcmp(token, ".resw") == 0 ){
	    /* It is a .resw directive, see how many words are going to be reserved. */
	    token = strtok(NULL, " :\n");
	    
	    /* Convert token from string to int. */
	    i = 0;
	    i = atoi(token);

	    /* Add this valueto our LC count*/
	    countLC += i;
	  }
	}
	}
      }
    }
  }


  /* END OF PASS ONE -- STARTING PASS TWO */


  /* Rewind file, go through each line searching for opcodes and operands. */
  rewind(inputFile);
  lineNum = 0;
  while(fgets(tempStr, MAX_LINE_LENGTH, inputFile) != NULL){
    /* Read in each line. */
    lineNum++;
    /* See if the first character is a '#' to indicate a comment line. */
    if(tempStr[0] != '#'){
      /* It is not a comment, check to see if it is a blank line. */
      token = strtok(tempStr, " :\n");
      if(token != NULL){
	/* It is not a blank line. */
	/* See if the first index is not blank. This will tell us if    */
	/* there is a label. If there is a label, we can conclude there */
	/* is going to be an opcode.                                    */
	if(tempStr[0] != ' '){
	  /* There is a character in the first index, we have a label.          */
	  /* strtok again to get an opcode. This could include .word or .resw   */
	  token = strtok(NULL, " \n");
	  /* Check if it is the .word or .resw directives. */
	  if(strcmp(token, ".resw") == 0){
	    /* The match is .resw directive.       */
	    /* Find out how many words to reserve. */
	    token = strtok(NULL, " \n");
	    j = atoi(token);
	    for(i = 0; i < j; i++){
	      objArray[objIndex] = 0;
	      objIndex++;
	    }
	    hadError = -1;
	  }
	  else if(strcmp(token, ".word") == 0){
	    /* The match is .word directive. */
	    /* Get the number we need to set the words to. */
	    token = strtok(NULL, " :\n");
	    j = atoi(token);
	    /* Get how many words to set to that number.   */
	    token = strtok(NULL, " \n");
	    for(i = 0; i < atoi(token); i++){
	      objArray[objIndex] = j;
	      objIndex++;
	    }
	    hadError = -1;
	  }
	  else{
	  /* We have an opcode. Check type and send it to appropriate function. */
	    for(i = 0; i < NUM_OP_CODES; i++){
	       if(strcmp(motStruct->opName[i], token) == 0){
	         /* We have found an opcode match. Send it to the right function. */
	         if(strcmp(motStruct->opType[i], "R") == 0){
		   /* R type instruction. */
		   objArray[objIndex] =  rInst(token, motStruct->opCode[i]);
	         }
	         else if(strcmp(motStruct->opType[i], "I") == 0){
                   /* I type instruction. */
	  	   objArray[objIndex] =  iInst(token, motStruct->opCode[i]);
		 
	         }
	         else if(strcmp(motStruct->opType[i], "J") == 0){
                   /* J type instruction. */
		   objArray[objIndex] =  jInst(token, motStruct->opCode[i], symbolList, &undefinedList);
		   /* Check if the undefinedList is still NULL, if it is not NULL, there was an error. */
		   if(undefinedList != NULL){
		     /* There was an error, flag the error. */
		     errorFlag = 1;
		   }
		 }
	         objIndex++;
	         /* Indicate we had made a match. */
	         hadError = -1;
	         break;
	       }
	    }
	  }
	  if(hadError != -1){
	    /* We didn't find an opcode match. Error in  */ 
	    /* the file. Put opcode in the unknown list. */
	    /* Check to see if the error list is empty. */
	    if(unknownList == NULL){
	      /* unknown list is empty, create a new node. */
	      if((unknownList = (struct errnode *)malloc(sizeof(struct errnode))) == NULL){
		fprintf(stderr, "errNode allocation failed.\n"); fflush(stderr);
		exit(1);
	      }

	      /* Store info in the error node. */
	      unknownList->lineNum = lineNum;
	      strncpy(unknownList->errStr, token, MAX_SYMBOL_LENGTH);
	    }
	    else{
	      /* Error list isn't empty, append new node. */
	      tempErrNode = unknownList;

	      /* Go to the end of the error list. */
	      for(; tempErrNode->next != NULL; tempErrNode = tempErrNode->next);

	      /* Create a new node. */
	      if((newErrNode = (struct errnode *)malloc(sizeof(struct errnode))) == NULL){
		fprintf(stderr, "errNode allocation failed.\n"); fflush(stderr);
		exit(1);
	      }

	      /* Put information in the new node. */
	      newErrNode->lineNum = lineNum;
	      strncpy(newErrNode->errStr, token, MAX_SYMBOL_LENGTH);

	      /* Put new node on the end of the list. */
	      tempErrNode->next = newErrNode;
	    }
	    errorFlag = 1;
	    hadError = 0;
	  }
	  hadError = 0;
	}
	else{
	  /* The first character is whitespace and the line isn't blank. */
	  /* This could either be an opcode or a directive.              */
	  /* Check token for .text or .data directives, if they don't    */
	  /* match we have an opcode.                                    */
	  if((strcmp(token, ".text") != 0) && (strcmp(token, ".data") != 0)){
	    /* We have an opcode. Loop through our mot struct to find     */
	    /* a match. Match the type and send it to the right function. */
	    for(i = 0; i < NUM_OP_CODES; i++){
	      if(strcmp(motStruct->opName[i], token) == 0){
	        /* We have found an opcode match. Send it to the right function. */
	        if(strcmp(motStruct->opType[i], "R") == 0){
	          /* R type instruction. */
	          objArray[objIndex] =  rInst(token, motStruct->opCode[i]);
	        }
	        else if(strcmp(motStruct->opType[i], "I") == 0){
	          /* I type instruction. */
	          objArray[objIndex] =  iInst(token, motStruct->opCode[i]);
		}
	        else if(strcmp(motStruct->opType[i], "J") == 0){
	          /* J type instruction. */
	          objArray[objIndex] =  jInst(token, motStruct->opCode[i], symbolList, &undefinedList);
		  
		  /* Check if the undefinedList is still NULL, if it is not NULL, there was an error. */
		  if(undefinedList != NULL){
		    /* There was an error, flag the error. */
		    errorFlag = 1;
		  }
	        }
	        objIndex++;
	        /* Indicate we had made a match. */
	        hadError = -1;
	        break;
	      }
	    }
	    if(hadError != -1){
	      /* We didn't find an opcode match. Error in  */
	      /* the file. Put opcode in the unknown list. */
	      /* Check to see if the list is empty.        */
	      /* Check to see if the error list is empty. */
	      if(unknownList == NULL){
	        /* unknown list is empty, create a new node. */
	        if((unknownList = (struct errnode *)malloc(sizeof(struct errnode))) == NULL){
	          fprintf(stderr, "errNode allocation failed.\n"); fflush(stderr);
	          exit(1);
	        }

	        /* Store info in the error node. */
	        unknownList->lineNum = lineNum;
	        strncpy(unknownList->errStr, token, MAX_SYMBOL_LENGTH);
	      }
	      else{
	        /* Error list isn't empty, append new node. */
	        tempErrNode = unknownList;

	        /* Go to the end of the error list. */
	        for(; tempErrNode->next != NULL; tempErrNode = tempErrNode->next);

	        /* Create a new node. */
	        if((newErrNode = (struct errnode *)malloc(sizeof(struct errnode))) == NULL){
	          fprintf(stderr, "errNode allocation failed.\n"); fflush(stderr);
	          exit(1);
	        }
	        /* Put information in the new node. */
	        newErrNode->lineNum = lineNum;
	        strncpy(newErrNode->errStr, token, MAX_SYMBOL_LENGTH);

	        /* Put new node on the end of the list. */
	        tempErrNode->next = newErrNode;
	      }
	      hadError = 0;
	      errorFlag = 1;
	    }
	    hadError = 0;
	  }
	}
      }	
    }
  }
  /* Passes are complete, check if any errors were found. */
  if(errorFlag == 1){
    /* There were errors, print out the error file. */
    printErr(duplicateList, undefinedList, unknownList, inputFile, argv[IN_FILE_ARG], filenameLength);
  }
  else{
    /* There were no errors. Print out the .sym and .obj files. */
    printSym(symbolList, argv[IN_FILE_ARG], filenameLength);
    printObj(objArray, objIndex, argv[IN_FILE_ARG], filenameLength);
  }


  /* Close input .asm file. */
  if(fclose(inputFile) == EOF){
    fprintf(stderr, "Error closing file %s\n", argv[IN_FILE_ARG]);
  }

  /* Exit program. */
  return (0);

}


void printSym(SYMNODE symbolList, char *inputFile, int filenameLength){
  
  /* Variables to create .sym file */
  char fileName[filenameLength+1];
  char *charPtr;
  SYMNODE tempNode = NULL;
  FILE *outputFile;

  /* Create our symbol filename. Take in our inputfile name and replace the extension with .sym. */
  strcpy(fileName, inputFile);
  charPtr = strrchr(fileName, '.');
  
  /* Replace the file extension. */
  *(charPtr+1) = 's';
  *(charPtr+2) = 'y';
  *(charPtr+3) = 'm';

  /* Open output .sym file for writing. */
  if((outputFile = fopen(fileName, "w")) == NULL) {
    /* Open Failed */
    fprintf(stderr, "File: %s could not be open for writing.\n", fileName); fflush(stderr);
    exit(1);
  }
  
  tempNode = symbolList;

  /* Loop through the symbolList and print all symbol data. */
  for(; tempNode != NULL; tempNode = tempNode->next){
    fprintf(outputFile, "%-16s %d\n", tempNode->symName, tempNode->lcValue); fflush(outputFile);
  }

  /* Close output .sym file. */
  if(fclose(outputFile) == EOF){
    fprintf(stderr, "Error closing file %s\n", fileName);
  }
}

void printObj(int objArray[65536], int objIndex, char *inputFile, int filenameLength){

  /* Variables to create .obj file */
  char fileName[filenameLength];
  char *charPtr;
  FILE *outputFile;
  int i = 0;

  /* Create our object filename. Take in our inputfile name and replace the extension with .obj. */
  strcpy(fileName, inputFile);
  charPtr = strrchr(fileName, '.');

  /* Replace the file extension. */
  *(charPtr+1) = 'o';
  *(charPtr+2) = 'b';
  *(charPtr+3) = 'j';

  /* Open output .obj file for writing. */
  if((outputFile = fopen(fileName, "w")) == NULL) {
    /* Open Failed */
    fprintf(stderr, "File: %s could not be open for writing.\n", fileName); fflush(stderr);
    exit(1);
  }
  
  /* Loop through our objArray and print the address (index) and hex value stored. */
  for(i = 0; i < objIndex; i++){
      fprintf(outputFile, "%2X %X\n", i, objArray[i]); fflush(outputFile);
  }

  /* Close output .obj file. */
  if(fclose(outputFile) == EOF){
    fprintf(stderr, "Error closing file %s\n", fileName);
  }
}

void printErr(ERRNODE duplicateList, ERRNODE undefinedList, ERRNODE unknownList, FILE *inputFile, char *inputFileName, int filenameLength){

  /* Variables to create .err file */
  char fileName[filenameLength+1];
  char *charPtr, tempStr[MAX_LINE_LENGTH];
  ERRNODE tempNode = NULL;
  FILE *outputFile;
  int lineCount = 1;

  /* Create our symbol filename. Take in our inputfile name and replace the extension with .err. */
  strcpy(fileName, inputFileName);
  charPtr = strrchr(fileName, '.');

  /* Replace the file extension. */
  *(charPtr+1) = 'e';
  *(charPtr+2) = 'r';
  *(charPtr+3) = 'r';

  /* Open output .err file for writing. */
  if((outputFile = fopen(fileName, "w")) == NULL) {
    /* Open Failed */
    fprintf(stderr, "File: %s could not be open for writing.\n", fileName); fflush(stderr);
    exit(1);
  }

  /* Print a source listing with numbered lines. */
  rewind(inputFile);
  while(fgets(tempStr, MAX_LINE_LENGTH, inputFile) != NULL){
    fprintf(outputFile, "%3d: %s", lineCount, tempStr); fflush(outputFile);
    lineCount++;
  }
  
  /* Line error header. */
  fprintf(outputFile, "\nError(s) Detected:\n"); fflush(outputFile);
  
  tempNode = unknownList;
  /* Cycle through unknown list printing illegal opcode errors. */
  for(; tempNode != NULL; tempNode = tempNode->next){
    fprintf(outputFile, "  Line: %3d  Illegal Opcode: %s\n", tempNode->lineNum, tempNode->errStr); fflush(outputFile);
  }
  
  /* Multiply Defined Symbol header. */
  fprintf(outputFile, "\nMultiply Defined Symbol(s):\n"); fflush(outputFile);
  /* Cycle though duplicate list printing each node. */
  tempNode = duplicateList;
  for(; tempNode != NULL; tempNode = tempNode->next){
    fprintf(outputFile, "  %s\n", tempNode->errStr); fflush(outputFile);
  }

  /* Undefined symbol header. */
  fprintf(outputFile, "\nUndefined Symbol(s):\n"); fflush(outputFile);
  
  /* Cycle though undefined list and print out nodes. */
  tempNode = undefinedList;
  for(; tempNode != NULL; tempNode = tempNode->next){
    fprintf(outputFile, "  %s\n", tempNode->errStr); fflush(outputFile);
  }

  /* Close output .sym file. */
  if(fclose(outputFile) == EOF){
    fprintf(stderr, "Error closing file %s\n", fileName);
  }
}
