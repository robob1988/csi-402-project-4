/* Robert Lowell RL659896 */
/* CSI402 Project 4       */
/* 4/15/13                */
/* rinst.c                */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "constants.h"
#include "struct_def.h"

/* Local prototypes. */
int mathOps(char *, char[]);
int moveOps(char *, char[]);
int shiftOps(char *, char[]);
int otherOps(char *, char[]);

int rInst(char *token, char opcode[OP_CODE_LENGTH]){
  /* Take in the token passed to us by finding an opcode match. */
  /* Send that token to the appropriate function for a hex      */
  /* instruction to be created.                                 */
  
  /* Check if the opcode is: add, sub, mul, div, mod, and, or, xor.   */
  /* All of these opcodes have the same instruction format and can be */
  /* made using the same function.                                    */

  if(strcmp(token, "add") == 0){
    return mathOps(token, opcode);
  }
  else if(strcmp(token, "sub") == 0){
    return mathOps(token, opcode);
  }
  else if(strcmp(token, "mul") == 0){
    return mathOps(token, opcode);
  }
  else if(strcmp(token, "div") == 0){
    return mathOps(token, opcode);
  }
  else if(strcmp(token, "mod") == 0){
    return mathOps(token, opcode);
  }
  else if(strcmp(token, "and") == 0){
    return mathOps(token, opcode);
  }
  else if(strcmp(token, "or") == 0){
    return mathOps(token, opcode);
  }
  else if(strcmp(token, "xor") == 0){
    return mathOps(token, opcode);
  }
  else if(strcmp(token, "move") == 0){
    return moveOps(token, opcode);
  }
  else if(strcmp(token, "com") == 0){
    return moveOps(token, opcode);
  }
  else if(strcmp(token, "sll") == 0){
    return shiftOps(token, opcode);
  }
  else if(strcmp(token, "srl") == 0){
    return shiftOps(token, opcode);
  }
  else if(strcmp(token, "sra") == 0){
    return shiftOps(token, opcode);
  }
  else if(strcmp(token, "jr") == 0){
    return otherOps(token, opcode);
  }
  else if(strcmp(token, "rdr") == 0){
    return otherOps(token, opcode);
  }
  else if(strcmp(token, "prr") == 0){
    return otherOps(token, opcode);
  }
  else if(strcmp(token, "prh") == 0){
    return otherOps(token, opcode);
  }
  else if(strcmp(token, "hlt") == 0){
    return 0;
  }
  /* Unimplemented will return 0 for now. */
  return 0;
}


int mathOps(char *token, char strOpcode[OP_CODE_LENGTH]){
  /* We have a token passed to us, it has 3 operands following. */
  /* Parse these operands and store them so we can construct a  */
  /* hex value to pass back.                                    */

  int hexInst = 0, opcode = 0;
  int targetReg = 0, sourceReg1 = 0, sourceReg2 = 0;
  
  /* Convert opcode to an int. */
  opcode = atoi(strOpcode);

  /* strtok the first operand which will be the target reg. */
  token = strtok(NULL, " $,\n");
  targetReg = atoi(token);
 
  /* strtok the second operand which will be first source reg. */
  token = strtok(NULL, " $,\n");
  sourceReg1 = atoi(token);

  /* strtok the third operand which will be the second source reg. */
  token = strtok(NULL, " $,\n");
  sourceReg2 = atoi(token);

  /* Using bit shifts and  masks, construct the hex instruction. */
  /* The format is: [6 Opcode][5 Rs1][5 Rs2][5 Rt][11 Unused]    */

  /* Shift the opcode into place; 32-6 = 26 to the left. */
  opcode = opcode << 26;
  /* Store the opcode into the hex instruction. */
  hexInst = (hexInst | opcode);

  /* Shift Rs1 into place; 26-5 = 21 to the left. */
  sourceReg1 = sourceReg1 << 21;
  /* Store Rs1 into the hex instruction. */
  hexInst = (hexInst | sourceReg1);

  /* Shift Rs2 into place; 21-5 = 16 to the left. */
  sourceReg2 = sourceReg2 << 16;
  /* Store Rs2 into the hex instruction. */
  hexInst = (hexInst | sourceReg2);
  
  /* Shift Rt into place; 16-5 = 11 to the left */
  targetReg = targetReg << 11;
  /* Store Rt into the hex instruction. */
  hexInst = (hexInst | targetReg);

  /* The remaining 11 bits are unused and set to 0. Return hexInst */
  
  return hexInst;

}


int moveOps(char *token, char strOpcode[OP_CODE_LENGTH]){
  /* We have a token passed to us, it has 2 operands following. */
  /* Parse these operands and store them so we can construct a  */
  /* hex value to pass back.                                    */

  int hexInst = 0, opcode = 0;
  int targetReg = 0, sourceReg1 = 0;

  /* Convert opcode to an int. */
  opcode = atoi(strOpcode);

  /* strtok the first operand which will be the target reg. */
  token = strtok(NULL, " $,\n");
  targetReg = atoi(token);

  /* strok the second operand which will be first source reg. */
  token = strtok(NULL, " $,\n");
  sourceReg1 = atoi(token);

  /* Using bit shifts and  masks, construct the hex instruction. */
  /* The format is: [6 Opcode][5 Rs1][5 Unused][5 Rt][11 Unused]    */

  /* Shift the opcode into place; 32-6 = 26 to the left. */
  opcode = opcode << 26;
  /* Store the opcode into the hex instruction. */
  hexInst = (hexInst | opcode);

  /* Shift Rs1 into place; 26-5 = 21 to the left. */
  sourceReg1 = sourceReg1 << 21;
  /* Store Rs1 into the hex instruction. */
  hexInst = (hexInst | sourceReg1);

  /* Shift Rt into place; 16-5 = 11 to the left */
  targetReg = targetReg << 11;
  /* Store Rt into the hex instruction. */
  hexInst = (hexInst | targetReg);

  /* Rs2 and remaning  11 bits are unused and set to 0. Return hexInst */
  return hexInst;
}


int shiftOps(char *token, char strOpcode[OP_CODE_LENGTH]){
  /* We have a token passed to us, it has 3 operands following. */
  /* Parse these operands and store them so we can construct a  */
  /* hex value to pass back.                                    */

  int hexInst = 0, opcode = 0;
  int targetReg = 0, sourceReg1 = 0, shiftAmt = 0;

  /* Convert opcode to an int. */
  opcode = atoi(strOpcode);

  /* strtok the first operand which will be the target reg. */
  token = strtok(NULL, " $,\n");
  targetReg = atoi(token);

  /* strtok the second operand which will be first source reg. */
  token = strtok(NULL, " $,\n");
  sourceReg1 = atoi(token);

  /* strtok the third operand which will be shift amount.  */
  token = strtok(NULL, " $,\n");
  shiftAmt = atoi(token);


  /* Using bit shifts and  masks, construct the hex instruction. */
  /* The format is: [6 Opcode][5 Rs1][5 Unused][5 Rt][5 Sa][6 Unused]    */

  /* Shift the opcode into place; 32-6 = 26 to the left. */
  opcode = opcode << 26;
  /* Store the opcode into the hex instruction. */
  hexInst = (hexInst | opcode);

  /* Shift Rs1 into place; 26-5 = 21 to the left. */
  sourceReg1 = sourceReg1 << 21;
  /* Store Rs1 into the hex instruction. */
  hexInst = (hexInst | sourceReg1);

  /* Shift Rt into place; 16-5 = 11 to the left */
  targetReg = targetReg << 11;
  /* Store Rt into the hex instruction. */
  hexInst = (hexInst | targetReg);

  /* Shift Sa into place; 11-5 = 6 to the left */
  shiftAmt = shiftAmt << 6;
  /* Store Rt into the hex instruction. */
  hexInst = (hexInst | shiftAmt);

  /* Rs2 and remaning  11 bits are unused and set to 0. Return hexInst */
  return hexInst;
}


int otherOps(char *token, char strOpcode[OP_CODE_LENGTH]){
  /* We have a token passed to us, it has 1 operand following.  */
  /* Parse the operand and store it so we can construct a       */
  /* hex value to pass back.                                    */

  int hexInst = 0, opcode = 0;
  int targetReg = 0;

  /* Convert opcode to an int. */
  opcode = atoi(strOpcode);

  /* strtok the operand which will be the target reg. */
  token = strtok(NULL, " $,\n");
  targetReg = atoi(token);

  /* Using bit shifts and  masks, construct the hex instruction. */
  /* The format is: [6 Opcode][5 Unused][5 Unused][5 Rt][11 Unused]    */

  /* Shift the opcode into place; 32-6 = 26 to the left. */
  opcode = opcode << 26;
  /* Store the opcode into the hex instruction. */
  hexInst = (hexInst | opcode);

  /* Shift Rt into place; 16-5 = 11 to the left */
  targetReg = targetReg << 11;
  /* Store Rt into the hex instruction. */
  hexInst = (hexInst | targetReg);

  /* Rs2 and remaning  11 bits are unused and set to 0. Return hexInst */
  return hexInst;
}
