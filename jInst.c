/* Robert Lowell RL659896 */
/* CSI402 Project 4       */
/* 4/15/13                */
/* jInst.c                */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "constants.h"
#include "struct_def.h"


/* Local prototypes. */
void undefinedNode(char *, ERRNODE *);

int loadAddrOp(char *, char[], SYMNODE, ERRNODE *);
int jumpOp(char *, char[], SYMNODE, ERRNODE *);
int jumpCondOp(char *, char[], SYMNODE, ERRNODE *);

int jInst(char *token, char opcode[OP_CODE_LENGTH], SYMNODE symbolList, ERRNODE *undefinedList){
  /* Take in the token passed to us by finding an opcode match. */
  /* Send that token to the appropriate function for a hex      */
  /* instruction to be created.                                 */

  /* Check if the opcode is: add, sub, mul, div, mod, and, or, xor.   */
  /* All of these opcodes have the same instruction format and can be */
  /* made using the same function.                                    */

  if(strcmp(token, "lwa") == 0){
    return loadAddrOp(token, opcode, symbolList, undefinedList);
  }
  else if(strcmp(token, "swa") == 0){
    return loadAddrOp(token, opcode, symbolList, undefinedList);
  }
  else if(strcmp(token, "j") == 0){
    return jumpOp(token, opcode, symbolList, undefinedList);
  }
  else if(strcmp(token, "jal") == 0){
    return jumpOp(token, opcode, symbolList, undefinedList);
  }
  else if(strcmp(token, "jeq") == 0){
    return jumpCondOp(token, opcode, symbolList, undefinedList);
  }
  else if(strcmp(token, "jne") == 0){
    return jumpCondOp(token, opcode, symbolList, undefinedList);
  }
  else if(strcmp(token, "jlt") == 0){
    return jumpCondOp(token, opcode, symbolList, undefinedList);
  }
  else if(strcmp(token, "jle") == 0){
    return jumpCondOp(token, opcode, symbolList, undefinedList);
  }
  else if(strcmp(token, "jgt") == 0){
    return jumpCondOp(token, opcode, symbolList, undefinedList);
  }
  else if(strcmp(token, "jge") == 0){
    return jumpCondOp(token, opcode, symbolList, undefinedList);
  }
 
  return 0;
}


int loadAddrOp(char *token, char strOpcode[OP_CODE_LENGTH], SYMNODE symbolList, ERRNODE *undefinedList){
  /* We have a token passed to us, it has 2 operands following. */
  /* Parse the operands and store them so we can construct a    */
  /* hex value to pass back.                                    */

  int hexInst = 0, opcode = 0;
  int targetReg = 0, lcAddr = 0;
  int matchFlag = 0;
  SYMNODE tempNode = NULL;

  tempNode = symbolList;

  /* Convert opcode to an int. */
  opcode = atoi(strOpcode);

  /* strtok the first operand which will be the target reg. */
  token = strtok(NULL, " $,\n");
  targetReg = atoi(token);

  /* strtok the second operand which will be a symbol name. */
  token = strtok(NULL, " ,\n");

  /* Search our symbol list for a match. */
  for(; tempNode != NULL; tempNode = tempNode->next){
    if(strcmp(token, tempNode->symName) == 0){
      /* We have found a match, get the LC value. */
      lcAddr = tempNode->lcValue;
      matchFlag = 1;
      break;
    }
  }

  /* Check if the match flag is 1, if not there was no matches. */
  if(matchFlag != 1){
    /* There were no matches. Add this symbol to the undefined List. */
    undefinedNode(token, undefinedList);
    return 0;
  }
  else{
    /* There was a match, reset the flag. */
    matchFlag = 0;
  }

  /* Using bit shifts and masks, construct the hex instruction.  */
  /* The format is: [6 Opcode][5 Unused][5 Rt][16 Address]       */

  /* Shift the opcode into place; 32-6 = 26 to the left. */
  opcode = opcode << 26;
  /* Store the opcode into the hex instruction. */
  hexInst = (hexInst | opcode);

  /* Shift Rt into place; 21-5 = 16 to the left */
  targetReg = targetReg << 16;
  /* Store Rt into the hex instruction. */
  hexInst = (hexInst | targetReg);

  /* Store the address in the 16 least significant bits. */
  hexInst = (hexInst | lcAddr);
  

  return hexInst;
}


int jumpOp(char *token, char strOpcode[OP_CODE_LENGTH], SYMNODE symbolList, ERRNODE *undefinedList){
  /* We have a token passed to us, it has 1 operand following.  */
  /* Parse the operand  and store it so we can construct a      */
  /* hex value to pass back.                                    */

  int hexInst = 0, opcode = 0;
  int lcAddr = 0, matchFlag = 0;
  SYMNODE tempNode = NULL;

  tempNode = symbolList;

  /* Convert opcode to an int. */
  opcode = atoi(strOpcode);

  /* strtok the operand which will be a symbol name. */
  token = strtok(NULL, " ,\n");

  /* Search our symbol list for a match. */
  for(; tempNode != NULL; tempNode = tempNode->next){
    if(strcmp(token, tempNode->symName) == 0){
      /* We have found a match, get the LC value. */
      lcAddr = tempNode->lcValue;
      matchFlag = 1;
      break;
    }
  }

  /* Check if the match flag is 1, if not there was no matches. */
  if(matchFlag != 1){
    /* There were no matches. Add this symbol to the undefined List. */
    undefinedNode(token, undefinedList);
    return 0;
  }
  else{
    /* There was a match, reset the flag. */
    matchFlag = 0;
  }

  /* Using bit shifts and masks, construct the hex instruction.  */
  /* The format is: [6 Opcode][5 Unused][5 Unused][16 Address]   */

  /* Shift the opcode into place; 32-6 = 26 to the left. */
  opcode = opcode << 26;
  /* Store the opcode into the hex instruction. */
  hexInst = (hexInst | opcode);

  /* Store the LC address in the 16 least significant bits. */
  hexInst = (hexInst | lcAddr);

  return hexInst;
}


int jumpCondOp(char *token, char strOpcode[OP_CODE_LENGTH], SYMNODE symbolList, ERRNODE *undefinedList){
  /* We have a token passed to us, it has 3 operands following. */
  /* Parse the operands  and store them so we can construct a   */
  /* hex value to pass back.                                    */

  int hexInst = 0, opcode = 0;
  int targetReg = 0, sourceReg = 0;
  int lcAddr = 0, matchFlag = 0;
  SYMNODE tempNode = NULL;

  tempNode = symbolList;

  /* Convert opcode to an int. */
  opcode = atoi(strOpcode);

  /* strtok the first operand which will be the target register. */
  token = strtok(NULL, " ,$\n");
  targetReg = atoi(token);

  /* strtok the second  operand which will be the source register. */
  token = strtok(NULL, " ,$\n");
  sourceReg = atoi(token);

  /* strtok the third operand which will be a symbol name. */
  token = strtok(NULL, " ,\n");

  /* Search our symbol list for a match. */
  for(; tempNode != NULL; tempNode = tempNode->next){
    if(strcmp(token, tempNode->symName) == 0){
      /* We have found a match, get the LC value. */
      lcAddr = tempNode->lcValue;
      matchFlag = 1;
      break;
    }
  }

  /* Check if the match flag is 1, if not there was no matches. */
  if(matchFlag != 1){
    /* There were no matches. Add this symbol to the undefined List. */
    undefinedNode(token, undefinedList);
    return 0;
  }
  else{
    /* There was a match, reset the flag. */
    matchFlag = 0;
  }

  /* Using bit shifts and masks, construct the hex instruction.  */
  /* The format is: [6 Opcode][5 Rs][5 Rt][16 Address]   */

  /* Shift the opcode into place; 32-6 = 26 to the left. */
  opcode = opcode << 26;
  /* Store the opcode into the hex instruction. */
  hexInst = (hexInst | opcode);

  /* Shift the Rs register into place; 26-5 = 21 to the left. */
  sourceReg = sourceReg << 21;
  /* Store the Rs into the hex instruction. */
  hexInst = (hexInst | sourceReg);

  /* Shift the Rt into place; 21-5 = 16 to the left. */
  targetReg = targetReg << 16;
  /* Store the Rt  into the hex instruction. */
  hexInst = (hexInst | targetReg);

  /* Store the LC address in the 16 least significant bits. */
  hexInst = (hexInst | lcAddr);

  return hexInst;
}


void undefinedNode(char *token, ERRNODE *undefinedList){
  /* Check the list to see if it is empty. If it is, create a new node and */
  /* add the undefined symbol to the list. If it is not, add the undefined */
  /* symbol to the end of the list.                                        */

  ERRNODE tempNode = NULL, newNode = NULL;

  /* Check if the list is empty. */
  if(*undefinedList == NULL){
    /* The list is empty, add a new node. */
    if((*undefinedList = (struct errnode *)malloc(sizeof(struct errnode))) == NULL){
      fprintf(stderr, "errNode allocation failed.\n"); fflush(stderr);
      exit(1);
    }

    /* Store the symbol in the list. */
    strncpy((*undefinedList)->errStr, token, MAX_SYMBOL_LENGTH);
  }
  else{
    /* The list is not empty. Go to the end of the list and store the new node. */
    tempNode = *undefinedList;
    for(; tempNode->next != NULL; tempNode = tempNode->next);

    if((newNode = (struct errnode *)malloc(sizeof(struct errnode))) == NULL){
      fprintf(stderr, "errNode allocation failed.\n"); fflush(stderr);
      exit(1);
    }
    
    /* Store the symbol in the new node. */
    strncpy(newNode->errStr, token, MAX_SYMBOL_LENGTH);

    /* Add new node to the end of the list. */
    tempNode->next = newNode;
  }
}
