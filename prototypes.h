/* Robert Lowell RL659896 */
/* CSI402 Project 4       */
/* 4/15/13                */
/* prototypes.h           */

#ifndef PROTOTYPES_H
#define PROTOTYPES_H

int rInst(char *, char[]);
int iInst(char *, char[]);
int jInst(char *, char[], SYMNODE, ERRNODE *);

#endif
