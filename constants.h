/* Robert Lowell RL659896 */
/* CSI402 Project 4       */
/* 4/15/13                */
/* constants.h            */

#ifndef CONSTANTS_H
#define CONSTANTS_H

#define NUM_ARGS 2
#define IN_FILE_ARG 1

#define MAX_LINE_LENGTH 80
#define MAX_SYMBOL_LENGTH 80

#define NUM_OP_CODES 36
#define OP_NAME_LENGTH 5
#define OP_CODE_LENGTH 3
#define TYPE_LENGTH 2

#define MOT_FILE "mot.txt"
#define NUM_R_OPS 18
#define NUM_I_OPS 8
#define NUM_J_OPS 10

#endif
