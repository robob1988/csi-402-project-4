/* Robert Lowell RL659896 */
/* CSI402 Project 4       */
/* 4/15/13                */
/* iInst.c                */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "constants.h"
#include "struct_def.h"

/* Local prototypes. */
int liOp(char *, char[]);
int iMathOp(char *, char[]);
int addrOp(char *, char[]);

int iInst(char *token, char opcode[OP_CODE_LENGTH]){
  /* Take in the token passed to us by finding an opcode match. */
  /* Send that token to the appropriate function for a hex      */
  /* instruction to be created.                                 */

  /* Check if the opcode is: add, sub, mul, div, mod, and, or, xor.   */
  /* All of these opcodes have the same instruction format and can be */
  /* made using the same function.                                    */

  if(strcmp(token, "li") == 0){
    return liOp(token, opcode);
  }
  if(strcmp(token, "addi") == 0){
    return iMathOp(token, opcode);
  }
  if(strcmp(token, "subi") == 0){
    return iMathOp(token, opcode);
  }
  if(strcmp(token, "muli") == 0){
    return iMathOp(token, opcode);
  }
  if(strcmp(token, "divi") == 0){
    return iMathOp(token, opcode);
  }
  if(strcmp(token, "modi") == 0){
    return iMathOp(token, opcode);
  }
  if(strcmp(token, "lwb") == 0){
    return addrOp(token, opcode);
  }
  if(strcmp(token, "swb") == 0){
    return addrOp(token, opcode);
  }

  return 0;
}


int liOp(char *token, char strOpcode[OP_CODE_LENGTH]){
  /* We have a token passed to us, it has 2 operands following. */
  /* Parse the operands and store them so we can construct a    */
  /* hex value to pass back.                                    */

  int hexInst = 0, opcode = 0;
  int targetReg = 0, loadInt = 0;
  unsigned short mask = 0xFFFF;

  /* Convert opcode to an int. */
  opcode = atoi(strOpcode);

  /* strtok the first operand which will be the target reg. */
  token = strtok(NULL, " $,\n");
  targetReg = atoi(token);

  /* strtok the second operand which will be an int constant. */
  token = strtok(NULL, " $,\n");
  loadInt = atoi(token);
  
  /* Using bit shifts and masks, construct the hex instruction.  */
  /* The format is: [6 Opcode][5 Unused][5 Rt][16 Immediate Operand]  */

  /* Shift the opcode into place; 32-6 = 26 to the left. */
  opcode = opcode << 26;
  /* Store the opcode into the hex instruction. */
  hexInst = (hexInst | opcode);

  /* Shift Rt into place; 21-5 = 16 to the left */
  targetReg = targetReg << 16;
  /* Store Rt into the hex instruction. */
  hexInst = (hexInst | targetReg);
  
  /* See if our input was a negative. */
  if(loadInt < 0){
    /* We are dealing with a negative number. */
    /* Set last 16 bits of the hex instruction to 1. Do this so when we */
    /* AND it with the input we can get the correct bits without        */
    /* overriding the most significant 16 bits of the int.              */

    hexInst = (hexInst | mask);
    hexInst = (hexInst & loadInt);
  }
  else{
    /* The integer is a positive number. Add it to the */
    /* last 16 bits of the hex instruction. Store      */
    /* immediate operand in the hex instruction.       */
    hexInst = (hexInst | loadInt);
  }  
  return hexInst;
}


int iMathOp(char *token, char strOpcode[OP_CODE_LENGTH]){
  /* We have a token passed to us, it has 3 operands following. */
  /* Parse the operands and store them so we can construct a    */
  /* hex value to pass back.                                    */

  int hexInst = 0, opcode = 0;
  int targetReg = 0, sourceReg = 0, loadInt = 0;
  unsigned short mask = 0xFFFF;

  /* Convert opcode to an int. */
  opcode = atoi(strOpcode);

  /* strtok the first operand which will be the target reg. */
  token = strtok(NULL, " $,\n");
  targetReg = atoi(token);

  /* strtok the second operand which will be the source reg. */
  token = strtok(NULL, " $,\n");
  sourceReg = atoi(token);

  /* strtok the second operand which will be an int constant. */
  token = strtok(NULL, " $,\n");
  loadInt = atoi(token);

  /* Using bit shifts and masks, construct the hex instruction.  */
  /* The format is: [6 Opcode][5 Rs][5 Rt][16 Immediate Operand]  */

  /* Shift the opcode into place; 32-6 = 26 to the left. */
  opcode = opcode << 26;
  /* Store the opcode into the hex instruction. */
  hexInst = (hexInst | opcode);

  /* Shift Rs into place; 26-5 = 21 to the left */
  sourceReg = sourceReg << 21;
  /* Store Rt into the hex instruction. */
  hexInst = (hexInst | sourceReg);

  /* Shift Rt into place; 21-5 = 16 to the left */
  targetReg = targetReg << 16;
  /* Store Rt into the hex instruction. */
  hexInst = (hexInst | targetReg);

  /* See if our input was a negative. */
  if(loadInt < 0){
    /* We are dealing with a negative number. */
    /* Set last 16 bits of the hex instruction to 1. Do this so when we */
    /* AND it with the input we can get the correct bits without        */
    /* overriding the most significant 16 bits of the int.              */

    hexInst = (hexInst | mask);
    hexInst = (hexInst & loadInt);
  }
  else{
    /* The integer is a positive number. Add it to the */
    /* last 16 bits of the hex instruction. Store      */
    /* immediate operand in the hex instruction.       */
    hexInst = (hexInst | loadInt);
  }
  return hexInst;
}


int addrOp(char *token, char strOpcode[OP_CODE_LENGTH]){
  /* We have a token passed to us, it has 3 operands following. */
  /* Parse the operands and store them so we can construct a    */
  /* hex value to pass back.                                    */

  int hexInst = 0, opcode = 0;
  int targetReg = 0, sourceReg = 0, loadInt = 0;
  unsigned short mask = 0xFFFF;

  /* Convert opcode to an int. */
  opcode = atoi(strOpcode);

  /* strtok the first operand which will be the target reg. */
  token = strtok(NULL, " $,\n");
  targetReg = atoi(token);

  /* strtok the second operand which will be an int constant. */
  token = strtok(NULL, " \($\\),\n");
  loadInt = atoi(token);

  /* strtok the third operand which will be the source operand. */
  token = strtok(NULL, " \($\\),\n");
  sourceReg = atoi(token);

  /* Using bit shifts and masks, construct the hex instruction.  */
  /* The format is: [6 Opcode][5 Rs][5 Rt][16 Immediate Operand]  */

  /* Shift the opcode into place; 32-6 = 26 to the left. */
  opcode = opcode << 26;
  /* Store the opcode into the hex instruction. */
  hexInst = (hexInst | opcode);

  /* Shift Rs into place; 26-5 = 21 to the left */
  sourceReg = sourceReg << 21;
  /* Store Rt into the hex instruction. */
  hexInst = (hexInst | sourceReg);

  /* Shift Rt into place; 21-5 = 16 to the left */
  targetReg = targetReg << 16;
  /* Store Rt into the hex instruction. */
  hexInst = (hexInst | targetReg);

  /* See if our input was a negative. */
  if(loadInt < 0){
    /* We are dealing with a negative number. */
    /* Set last 16 bits of the hex instruction to 1. Do this so when we */
    /* AND it with the input we can get the correct bits without        */
    /* overriding the most significant 16 bits of the int.              */

    hexInst = (hexInst | mask);
    hexInst = (hexInst & loadInt);
  }
  else{
    /* The integer is a positive number. Add it to the */
    /* last 16 bits of the hex instruction. Store      */
    /* immediate operand in the hex instruction.       */
    hexInst = (hexInst | loadInt);
  }
  return hexInst;
}
